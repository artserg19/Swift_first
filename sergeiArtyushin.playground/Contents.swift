import UIKit
import Foundation

//MARK: First Lesson
var name = "Артюшин Сергей"
print(name)

//MARK: Second Lesson
// Task 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Task 2
//До внесения изменения из задания 3 было: let milkPrice: Int = 3
var milkPrice: Double = 3

//Task 3
milkPrice = 4.20

//Task 4
var milkBottleCount : Int? = 20
var profit: Double = 0.0
if let bottleCount = milkBottleCount {
    profit = (milkPrice * Double(bottleCount))
    print(profit)
}
/* Форс анрап нужно опасаться поскольку существует вероятность,
 что в ходе выполнения программы значение в опциональной переменной может не появится и её состояние останется nil,
 тогда как мы дойдем до сполнения кода связанного с этой переменной приложение упадет */

//Task 5
var employeesList = ["Петр","Геннадия","Ивана","Марфа","Андрей"]

//Task 6
var isEveryoneWorkHard = false
var workingHours = 39

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)

//MARK: Third Lesson Task 1
func makeBuffer() -> (String) -> Void {
    var storgeString = ""

    func executor(str: String) {
        switch (str) {
        case "":
            print(storgeString)
        default:
            storgeString += str
        }
    }
    return executor
}

var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")

buffer("нужно!")
buffer(" нужно!")
buffer("")

//MARK: Third Lesson Task 2
func checkPrimeNumber(_ number: Int) -> Bool {
    guard number >= 2 else { return false }

    for _ in 2 ..< number {
        if number % 2 == 0 {
            return false
        }
    }
    return true
}

checkPrimeNumber(7)
checkPrimeNumber(8)
checkPrimeNumber(13)

//MARK: Fifth Lesson Task 1
class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }
    
    func calculatePerimetr() -> Double {
        fatalError("not implemented")
    }
}

final class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        return height * width
    }
    
    override func calculatePerimetr() -> Double {
        return (height + width) * 2
    }
}

final class Circle: Shape {
    private let radius: Double
    private let pi = 3.14
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        return pi * pow(radius, 2)
    }
    
    override func calculatePerimetr() -> Double {
        return 2 * pi * radius
    }
}

final class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        return pow(side, 2)
    }
    
    override func calculatePerimetr() -> Double {
        return side * 4
    }
}

var rect = Rectangle(height: 3, width: 3)
var square = Square(side: 5)
var circle = Circle(radius: 3)

var shapes: [Shape]
shapes = [rect, square, circle]

for shape in shapes {
    if let shape = shape as? Rectangle {
        print("Площадь прямоугольника = \(shape.calculateArea())")
        print("Периметр прямоугольника = \(shape.calculatePerimetr())")
    } else if let shape = shape as? Square {
        print("Площадь квадрата = \(shape.calculateArea())")
        print("Периметр квадрата = \(shape.calculatePerimetr())")
    } else if let shape = shape as? Circle {
        print("Площадь круга = \(shape.calculateArea())")
        print("Периметр круга = \(shape.calculatePerimetr())")
    }
}
